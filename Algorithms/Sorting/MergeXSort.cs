﻿using Algorithms.Sorting.ElementarySort;
using System;

namespace Algorithms.Sorting
{
    public class MergeXSort : BaseSort
    {
        private MergeXSort() { }

        private static int CUTOFF = 7;

        private static void Merge(IComparable[] src, IComparable[] dst, int lo, int mid, int hi)
        {

            int i = lo, j = mid + 1;
            for (int k = lo; k <= hi; k++)
            {
                if (i > mid) dst[k] = src[j++];
                else if (j > hi) dst[k] = src[i++];
                else if (Less(src[j], src[i])) dst[k] = src[j++];
                else dst[k] = src[i++];
            }
        }

        private static void Sort(IComparable[] src, IComparable[] dst, int lo, int hi)
        {
            if (hi <= lo + CUTOFF)
            {
                InsertionSort(dst, lo, hi);
                return;
            }
            int mid = lo + (hi - lo) / 2;
            Sort(dst, src, lo, mid);
            Sort(dst, src, mid + 1, hi);

            if (!Less(src[mid + 1], src[mid]))
            {
                for (int i = lo; i <= hi; i++) dst[i] = src[i];
                return;
            }

            Merge(src, dst, lo, mid, hi);
        }

        public static void Sort(IComparable[] a)
        {
            IComparable[] aux = (IComparable[])a.Clone();
            Sort(aux, a, 0, a.Length - 1);
        }

        private static void InsertionSort(IComparable[] a, int lo, int hi)
        {
            for (int i = lo; i <= hi; i++)
                for (int j = i; j > lo && Less(a[j], a[j - 1]); j--)
                    Exchange(a, j, j - 1);
        }

        private new static bool IsSorted(IComparable[] a)
        {
            return IsSorted(a, 0, a.Length - 1);
        }

        private static bool IsSorted(IComparable[] a, int lo, int hi)
        {
            for (int i = lo + 1; i <= hi; i++)
                if (Less(a[i], a[i - 1])) return false;
            return true;
        }
    }
}
