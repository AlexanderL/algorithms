﻿using System.Text;

namespace Algorithms.UnionFind
{
    public class QuickUnionUf
    {
        public QuickUnionUf(int N)
        {
            id = new int[N];
            for (int i = 0; i < id.Length; i++)
            {
                id[i] = i;
            }
        }

        private int[] id;

        private int GetRoot(int i)
        {
            while (i != id[i])
            {
                i = id[i];
            }

            return i;
        }

        public bool IsConnected(int a, int b)
        {
            return GetRoot(a) == GetRoot(b);
        }

        public void Union(int a, int b)
        {
            int i = GetRoot(a);
            int j = GetRoot(b);
            id[i] = j;
        }

        public override string ToString()
        {
            var sb = new StringBuilder(id.Length);
            foreach (var item in id)
            {
                sb.Append(item);
            }
            return sb.ToString();
        }
    }
}
