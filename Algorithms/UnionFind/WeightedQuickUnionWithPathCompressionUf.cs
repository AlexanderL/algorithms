﻿using System.Text;

namespace Algorithms.UnionFind
{
    public class WeightedQuickUnionWithPathCompressionUf
    {
        public WeightedQuickUnionWithPathCompressionUf(int N)
        {
            Count = N;
            id = new int[Count];
            sz = new int[Count];
            for (int i = 0; i < Count; i++)
            {
                id[i] = i;
                sz[i] = 1;
            }
        }

        private int[] id;
        private int[] sz;

        public int Count { get; private set; }

        private int GetRoot(int i)
        {
            while (i != id[i])
            {
                id[i] = id[id[i]];
                i = id[i];
            }

            return i;
        }

        public bool IsConnected(int a, int b)
        {
            return GetRoot(a) == GetRoot(b);
        }

        public void Union(int a, int b)
        {
            int i = GetRoot(a);
            int j = GetRoot(b);
            if (i == j) return;

            if (sz[i] < sz[j]) { id[i] = j; sz[j] += sz[i]; }
            else { id[j] = i; sz[i] += sz[j]; }
            Count--;
        }

        public override string ToString()
        {
            var sb = new StringBuilder(id.Length);
            foreach (var item in id)
            {
                sb.Append(item);
            }
            return sb.ToString();
        }
    }
}
