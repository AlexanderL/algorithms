﻿using System.Text;

namespace Algorithms.UnionFind
{
    public class QuickFindUf
    {
        public QuickFindUf(int N)
        {
            id = new int[N];
            for (int i = 0; i < N; i++)
                id[i] = i;
        }

        private int[] id;

        public bool IsConnected(int a, int b)
        {
            return id[a] == id[b];
        }

        public void Union(int a, int b)
        {
            int aId = id[a];
            int bId = id[b];
            for (int i = 0; i < id.Length; i++)
            {
                if (id[i] == aId)
                    id[i] = bId;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder(id.Length);
            foreach (var item in id)
            {
                sb.Append(item);
            }
            return sb.ToString();
        }
    }
}
