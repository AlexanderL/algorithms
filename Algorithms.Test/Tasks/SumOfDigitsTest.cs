﻿using Algorithms.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Algorithms.Test.Tasks
{
    [TestClass]
    public class SumOfDigitsTest
    {
        [TestMethod]
        public void ZeroTest()
        {
            var s = SumOfDigits.GetSum(0);
            Assert.AreEqual(s, 0);
        }

        [TestMethod]
        public void OneDigitTest()
        {
            var s = SumOfDigits.GetSum(5);
            Assert.AreEqual(s, 5);
        }

        [TestMethod]
        public void OneNegativeDigitTest()
        {
            var s = SumOfDigits.GetSum(-5);
            Assert.AreEqual(s, 5);
        }

        [TestMethod]
        public void TwoDigitTest()
        {
            var s = SumOfDigits.GetSum(55);
            Assert.AreEqual(s, 10);
        }

        [TestMethod]
        public void TwoNegativeDigitTest()
        {
            var s = SumOfDigits.GetSum(-55);
            Assert.AreEqual(s, 10);
        }
    }
}
