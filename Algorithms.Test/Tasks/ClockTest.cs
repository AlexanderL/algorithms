﻿using Algorithms.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Algorithms.Test.Tasks
{
    [TestClass]
    public class ClockTest
    {
        [TestMethod]
        public void ClockZeroTest()
        {
            int c = Clock.GetMinutesToNextIntersection(0, 0);
            Assert.AreEqual(c, 0);
        }

        [TestMethod]
        public void ClockOneTest()
        {
            int c = Clock.GetMinutesToNextIntersection(1, 1);
            Assert.AreEqual(c, 4);
        }
    }
}
