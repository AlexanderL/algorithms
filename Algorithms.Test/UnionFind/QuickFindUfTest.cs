﻿using Algorithms.UnionFind;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace Algorithms.Test.UnionFind
{
    [TestClass]
    public class QuickFindUfTest
    {
        [TestMethod]
        public void Tiny()
        {
            var uf = Process("tinyUF.txt");
            Assert.IsTrue(Data.IsEqual("tinyUF_result.txt", uf.ToString()));
        }

        [TestMethod]
        public void Medium()
        {
            var watch = Stopwatch.StartNew();
            var uf = Process("mediumUF.txt");
            Assert.IsNotNull(uf);

            watch.Stop();
            Trace.WriteLine("QuickFindUfTest, Medium test elapsed: " + watch.Elapsed.ToString());
        }

        /*
        [TestMethod]
        public void Large()
        {
            var watch = Stopwatch.StartNew();
            var uf = Process("largeUF.txt");
            Assert.IsNotNull(uf);

            watch.Stop();
            Trace.WriteLine("QuickFindUfTest, Large test elapsed: " + watch.Elapsed.ToString());
        }
        */

        private QuickFindUf Process(string fileName)
        {
            var d = new Data(fileName);
            int N = d.ReadNextInt();
            var uf = new QuickFindUf(N);

            while (d.CanReadNextLine)
            {
                var a = d.ReadNextInt();
                var b = d.ReadNextInt();
                if (uf.IsConnected(a, b))
                    continue;
                uf.Union(a, b);
            }

            return uf;
        }

        [TestCleanup]
        public void Cleanup()
        {
            Trace.Flush();
        }
    }
}
