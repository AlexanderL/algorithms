﻿using Algorithms.UnionFind;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace Algorithms.Test.UnionFind
{
    [TestClass]
    public class WeightedQuickUnionUfTest
    {
        [TestMethod]
        public void Tiny()
        {
            var uf = Process("tinyUF.txt");
            Assert.IsTrue(Data.IsEqual("tinyUF_result_weightedQuickUnion.txt", uf.ToString()));
        }

        [TestMethod]
        public void Medium()
        {
            var watch = Stopwatch.StartNew();
            var uf = Process("mediumUF.txt");
            Assert.IsNotNull(uf);

            watch.Stop();
            Trace.WriteLine("WeightedQuickUnionUfTest, Medium test elapsed: " + watch.Elapsed.ToString());
        }

        [TestMethod]
        public void Large()
        {
            var watch = Stopwatch.StartNew();
            var uf = Process("largeUF.txt");
            Assert.IsNotNull(uf);

            watch.Stop();
            Trace.WriteLine("WeightedQuickUnionUfTest, Large test elapsed: " + watch.Elapsed.ToString());
        }

        private WeightedQuickUnionUf Process(string fileName)
        {
            var d = new Data(fileName);
            int N = d.ReadNextInt();
            var uf = new WeightedQuickUnionUf(N);

            while (d.CanReadNextLine)
            {
                var a = d.ReadNextInt();
                var b = d.ReadNextInt();
                if (uf.IsConnected(a, b))
                    continue;
                uf.Union(a, b);
            }

            return uf;
        }

        [TestCleanup]
        public void Cleanup()
        {
            Trace.Flush();
        }
    }
}
