﻿using Algorithms.Sorting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Test.Sorting
{
    [TestClass]
    public class MergeSortTest
    {
        void InitTiny()
        {
            a = new Data("tiny.txt").ReadAndSplitLines();
            aSorted = new Data("tiny_result.txt").ReadAndSplitLines();
        }

        void InitWords()
        {
            a = new Data("words3.txt").ReadAndSplitLines();
            aSorted = new Data("words3_result.txt").ReadAndSplitLines();
        }

        string[] a;
        string[] aSorted;

        [TestMethod]
        public void MergeSortTinyTest()
        {
            InitTiny();
            MergeSort.Sort(a);

            Assert.IsTrue(a.SequenceEqual(aSorted));
        }

        [TestMethod]
        public void MergeSortWordsTest()
        {
            InitWords();
            MergeSort.Sort(a);

            Assert.IsTrue(a.SequenceEqual(aSorted));
        }

        [TestMethod]
        public void MergeXSortTinyTest()
        {
            InitTiny();
            MergeXSort.Sort(a);

            Assert.IsTrue(a.SequenceEqual(aSorted));
        }

        [TestMethod]
        public void MergeXSortWordsTest()
        {
            InitWords();
            MergeXSort.Sort(a);

            Assert.IsTrue(a.SequenceEqual(aSorted));
        }
    }
}
