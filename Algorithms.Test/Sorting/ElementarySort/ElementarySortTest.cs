﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Algorithms.Sorting.ElementarySort;
using System.Linq;

namespace Algorithms.Test.Sorting.ElementarySort
{
    [TestClass]
    public class ElementarySortTest
    {
        [TestInitialize]
        public void Init()
        {
            a = new Data("tiny.txt").ReadAndSplitLines();
            aSorted = new Data("tiny_result.txt").ReadAndSplitLines();
        }

        string[] a;
        string[] aSorted;

        [TestMethod]
        public void SelectionSortTest()
        {
            Selection.Sort(a);

            Assert.IsTrue(a.SequenceEqual(aSorted));
        }

        [TestMethod]
        public void InsertionSortTest()
        {
            Insertion.Sort(a);

            Assert.IsTrue(a.SequenceEqual(aSorted));
        }
    }
}
