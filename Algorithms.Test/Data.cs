﻿using System;
using System.IO;

namespace Algorithms.Test
{
    internal class Data : IDisposable
    {
        internal static string DataPath = @".\Data\";

        public Data(string fileName)
        {
            _sr = new StreamReader(DataPath + fileName);
        }

        public void Dispose()
        {
            if (_sr != null)
            {
                _sr.Close();
                _sr = null;
            }
        }

        StreamReader _sr;
        int _indexInLine;
        string[] _splitLines;

        internal bool CanReadNextLine
        {
            get
            {
                return !_sr.EndOfStream;
            }
        }

        internal int ReadNextInt()
        {
            if (_splitLines == null)
                SplitStringBySpace(_sr.ReadLine());

            var result = int.Parse(_splitLines[_indexInLine]);
            _indexInLine++;

            if (_indexInLine >= _splitLines.Length)
            {
                _indexInLine = 0;
                _splitLines = null;
            }

            return result;
        }

        internal string[] ReadAndSplitLines()
        {
            SplitStringBySpace(_sr.ReadToEnd());
            return _splitLines;
        }

        private void SplitStringBySpace(string stringToSplit)
        {
            _splitLines = stringToSplit.Split(new char[] { ' ', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        }

        internal static bool IsEqual(string filename, string stringToCompare)
        {
            var s = File.ReadAllText(DataPath + filename);
            return s.Equals(stringToCompare);
        }
    }
}
