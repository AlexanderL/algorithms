﻿namespace Algorithms.Tasks
{
    public static class Clock
    {
        /// <summary>
        /// Returns interval (in minutes) to the next arrows intersection.
        /// </summary>
        /// <param name="h">Hours</param>
        /// <param name="m">Minutes</param>
        /// <returns></returns>
        public static int GetMinutesToNextIntersection(int h, int m)
        {
            int result;
            result = (11 * (60 * h + m)) % 720;
            if (result != 0)
                result = 720 - result;

            return result / 11;
        }
    }
}
