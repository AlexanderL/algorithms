﻿using System;

namespace Algorithms.Tasks
{
    public static class SumOfDigits
    {
        /// <summary>
        /// Return sum of digits in number
        /// </summary>
        /// <param name="number">Number</param>
        /// <returns></returns>
        public static int GetSum(int number)
        {
            number = Math.Abs(number);
            int result = 0;
            while (number!=0)
            {
                result += number % 10;
                number = number / 10;
            }

            return result;
        }
    }
}
